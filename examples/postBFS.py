from pyfoamutils import post





# This is the slowest and most intesive part of the program. 
# 
# The following command loads reads and loads the mesh into the bfs_90 object. The argument `caseLocation` defaults to the current working directory.
# 
# Additionally, the `xRegion` argument allows the user to specify a region of interest, for instance the inlet channel $x = [0, 6.4]$. This feature assumes a homogeneous geometry.
# 
# If one wishes to process the entire BFS channel then the xRegion feature should NOT be used, like so:
# 



#bfs_90  =  post.Mesh()


# Alternatively for the starter channel


bfs_90_channel  =  post.Mesh(xRegion=[0, 6.4])


# Initially both methods read the same 3D mesh  (This may change in the future)



print("3D Mesh Cell Count: " , bfs_90_channel.cellCount)


# The 2D mesh cell count

print("2D Mesh Cell Count: ", len(bfs_90_channel.cell2DArr))


# Build fields list

fields = ['UMean', 'Rm','Prod', 'Diss', 'pStrain', 'pDiff', 'pTrans', 'tTrans', 'mDiff']




# loop through fields at time 126.69997
# sample fields at x = 3.2 and print .xy plot files
# average region and print .xy plot files
for f in fields:
    field  = post.Field(time = 126.69997, field = f)
    
    print (field.header)

    field.fetchField()

    field.spanwiseAvg(bfs_90_channel.spanDict)

    # Sample fields at x = 3.2, data is stored at field.lineData
    field.extractLine(3.2, bfs_90_channel.cell2DArr)
    # write out the sampled field at ./plots/
    field.writePlotData(labelStyle='xy', saveLocation='plots')
    # Average the domain in the streamwise and spanwise direction, data is stored at field.avgLineField
    field.averagedField(field.fieldData2D, bfs_90_channel.streamDict, bfs_90_channel.averagedGrid)
    # write out the sampled field at ./plots/avg/
    field.writePlotData(lineData = field.avgLineField, labelStyle='xy', positionLabel=False ,saveLocation='plots/avg')



