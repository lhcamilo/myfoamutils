#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: leo
"""

import pkg_resources as pkg


def readChannelBlockMesh(fixedMass = False):
    """
Reads the the template files located at the channelDocs folder for the 
channel case.

Input:
    fixedMass (boolean):    Switch between fixed mass and fixed 
                            pressure gradient templates.
Output:
    blockMeshTop (string):  Template of the top part of the blockMeshDict
        
    blockMeshBottom (string): Template of the bottom part of the blockMeshDict
    """    
    
    location = 'channelDocs/blockMeshTop'
    with open(pkg.resource_filename(__name__,location),"r") as myFile:
        blockMeshTop = myFile.read()
    
    if fixedMass:    
        location = 'channelDocs/blockMeshBottomMapped'
        with open(pkg.resource_filename(__name__,location),"r") as myFile:
            blockMeshBottom = myFile.read()    
    else:
        location = 'channelDocs/blockMeshBottomCyclic'
        with open(pkg.resource_filename(__name__,location),"r") as myFile:
            blockMeshBottom = myFile.read()    
            
    return blockMeshTop, blockMeshBottom
            
def readConjChannelBlockMesh(fixedMass = False):
    """
Reads the the template files located at the channelDocs folder for the 
conjugate heat channel case.

Input:
    fixedMass (boolean):    Switch between fixed mass and fixed 
                            pressure gradient templates.
Output:
    blockMeshTop (string):  Template of the top part of the blockMeshDict
        
    blockMeshBottom (string): Template of the bottom part of the blockMeshDict
    """
    
    location = "channelDocs/blockMeshTop"
    with open(pkg.resource_filename(__name__,location),"r") as myFile:
        blockMeshTop = myFile.read()
        myFile.close()
        
    if fixedMass:    
        location = 'channelDocs/blockMeshConjBottomMapped'
        with open(pkg.resource_filename(__name__,location),"r") as myFile:
            blockMeshBottom = myFile.read()    
            myFile.close()    
    else:
        location = 'channelDocs/blockMeshConjBottomMapped'
        with open(pkg.resource_filename(__name__,location),"r") as myFile:
            blockMeshBottom = myFile.read()    
            myFile.close() 
            
    return blockMeshTop, blockMeshBottom

               
def blockMeshHeader(retau, cellCount, cellSizes, lengths, stretchRatio,
                    fixedMass = False):
    """
Aggregates and formats the input header string to build a channel case.

    
Input:  retau (int):                Frictional reynolds number
        cellCount (list[float]):    Cell count for each direction/region
        cellSizes (list[float]):    Min, max  cell size for each 
                                    direction/region
        lengths (list[float]):      List of relevant lengths used for block 
                                    building for each direction/region
        stretchRatio (float):       Ratio between max/min cell sizes at the
                                    fluid and solid expansion regions.
        fixedMass (boolean):        Switch between fixed mass and fixed 
                                    pressure gradient templates.
    
Output:
        Configuration header (string)
    """
    
    fluidCellCount = cellCount[0] * cellCount[1][2] * cellCount[2]

    if fixedMass:
        
        fString = "// Fixed Mass Flow" 
        
    else:
            
        fString = "// Fixed Pressure Gradient" 
        
                       

    return \
"""

//-----------------------------------------------------------------------
//                         Mesh Summary

// Re_tau = {retau}

// ({x:.1f}, {ymin:.1f} - {ymax:.1f}, {z:.1f} )

// Total Cell Count = {totalCellCount:.0f}

{fString}

//-----------------------------------------------------------------------

x1 {x1:.4f};\t\t//Max coord of the channel in the x-direction

y1 {y1:.4f};\t\t//height of bottom boundary layer
y2 {y2:.4f};\t\t//height of starting point of the upper boundary layer
y3 {y3:.4f};\t\t//height of of the channel in the y-direction

z1 {z1:.4f};\t\t//length of the channel in the z-direction

gryr1  {strtch:.4f};\t\t\t//growth ratio of the first BL
gryr1i {strtchI:.4f};\t\t\t//inverse of the first BL growth ratio, top BL


nx {nx};\t\t\t// number of cells in the x-direction
ny1 {ny1};\t\t// Number of cells in the expansion layer of the fluid
ny2 {ny2};\t\t\t// Number of cells in the bulk region of the fluid 
nz {nz};\t\t\t// Number of cells in the z-direction
    """.format(retau = retau, x=cellSizes[0], ymin=min(cellSizes[1]),
             ymax = max(cellSizes[1]), z = cellSizes[2],
             totalCellCount = fluidCellCount, fString = fString, 
             x1 = lengths[0], y1 = lengths[1][0], y2 = lengths[1][1],
             y3 = lengths[1][2], z1 = lengths[2], strtch = stretchRatio,
             strtchI=stretchRatio**-1, nx = cellCount[0], ny1 = cellCount[1][0],
             ny2 = cellCount[1][1], nz = cellCount[2])



def blockMeshConjHeader(retau, cellCount, cellSizes, lengths, stretchRatio,
                    fixedMass = False):
    """
Aggregates and formats the input header string to build a conjugate heat 
channel case.

    
Input:  retau (int):                frictional reynolds number
        cellCount (list[float]):    cell count for each direction/region
        cellSizes (list[float]):    min, max  cell size for each 
                                    direction/region, (only the fluid region 
                                    is relevant)
        lengths (list[float]):      list of relevant lengths used for block 
                                    building for each direction/region
        stretchRatio (list[float]): ratio between max/min cell sizes at the
                                    fluid and solid expansion regions.
        fixedMass (boolean):        Switch between fixed mass and fixed 
                                    pressure gradient templates.
    
Output:
        Configuration header (string)
    """
    
    fluidCellCount = cellCount[0] * cellCount[1][2] * cellCount[2]
    solidCellCount = 2*cellCount[0] * cellCount[3][2] * cellCount[2]
    totalCellCount = fluidCellCount + solidCellCount

    if fixedMass:
        
        fString = "// Fixed Mass Flow" 
        #fName = f"channelCase/blockMeshDict_g{grid}_fixedMass"
        
    else:
            
        fString = "// Fixed Pressure Gradient" 
        #fName = f"channelCase/blockMeshDict_g{grid}_fixedPress"
        
        totalCellCount = cellCount[0] * cellCount[1][2] * cellCount[2]
                       

    return \
"""

//-----------------------------------------------------------------------
//                         Mesh Summary


// Re_tau = {retau}

// ({x:.1f}, {ymin:.1f} - {ymax:.1f}, {z:.1f} )

// Solid Region Cell Count = {solidCellCount:.0f}

// Fluid Region Cell Count = {fluidCellCount:.0f}

// Total Cell Count = {totalCellCount:.0f}


//-----------------------------------------------------------------------
x1 {x1:.4f};\t\t//length of the channel in the x-direction
y1 {y1:.4f};\t\t//height of bottom boundary layer
y2 {y2:.4f};\t\t//height of starting point of the upper boundary layer
y3 {y3:.4f};\t\t//height of of the channel in the y-direction
y4 {y4:.4f};\t\t//height of top solid boundary layer
y5 {y5:.4f};\t\t//Maximum height of top solid of the channel
y6 {y6:.4f};\t\t//height of bottom solid boundary layer
y7 {y7:.4f};\t\t//Minimum height of bottom solid of the channel
z1 {z1:.4f};\t\t//length of the channel in the z-direction

gryr1  {strtch1:.4f};\t\t//growth ratio of the fluid BL
gryr1i {strtch1I:.4f};\t\t//inverse of the fluid BL growth ratio, top BL
gryr2  {strtch2:.4f};\t\t//growth ratio of the wall BL
gryr2i {strtch2I:.4f};\t\t//inverse of the wall BL growth ratio, top BL

nx {nx};\t\t\t// number of cells in the x-direction
ny1 {ny1};\t\t\t// Number of cells in the expansion layer of the fluid
ny2 {ny2};\t\t\t// Number of cells in the bulk region of the fluid 
ny3 {ny3};\t\t\t// Number of cells in the expansion layer of the solid
ny4 {ny4};\t\t\t// Number of cells in the bulk region of the solid 
nz {nz};\t\t\t// Number of cells in the z-direction
""".format(retau = retau, x=cellSizes[0], ymin=min(cellSizes[1]),
         ymax = max(cellSizes[1]), z = cellSizes[2],
         totalCellCount = totalCellCount,fluidCellCount=fluidCellCount,
         solidCellCount=solidCellCount, fString = fString,
         x1 = lengths[0],
         y1 = lengths[1][0], y2 = lengths[1][1],y3 = lengths[1][2],
         y4 = lengths[1][3], y5 = lengths[1][4],y6 = lengths[1][5],
         y7 = lengths[1][6], z1 = lengths[2], 
         strtch1 = stretchRatio[0], strtch1I=stretchRatio[0]**-1,
         strtch2 = stretchRatio[1], strtch2I=stretchRatio[1]**-1,
         nx = cellCount[0], ny1 = cellCount[1][0],
         ny2 = cellCount[1][1], ny3 = cellCount[3][0],
         ny4 = cellCount[3][1], nz = cellCount[2])
