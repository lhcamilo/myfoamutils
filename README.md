# pyFoamUtils

Collection of python utilities used for pre- and post- processing of:

* Turbulent channel flow simulations
* Turbulent conjugate heat channel flow simulations
* Backwards-Facing Step (BFS) 

gitlab repo at : https://gitlab.com/lhcamilo/myfoamutils

# Installation

At the moment the package can only be installed locally. In order to install it, clone the repo, cd to the project directory and use pip to install it:

    pip install -e .


## Pre-processing module

The pyfoamutils.pre  has classes to generate the blockMeshDict file to be read by OpenFOAM's blockMesh utility. The module also contains functions used in the pre-processing phase. **Some of these functions and objects are exemplified at the jupyter notebook at examples/preExample.ipynb.**

## Post-processing module

The aim of this module is mainly to peform the averaging of fields of channel and BFS geometries and collect datasets of specific locations. The chosen directions need to be uniform in cell size and the fields need to be homogeneous (non-varying). 

OpenFOAM stores its grid information on file inside the file at the `caseDirectory/constant/polyMesh/` folder. The files used by the post processing utility are

* points - list of coordinates of each point
* faces - list of points in each face 
* owner - list of cells for each face

The location of the case directory is input into the `pre.Mesh` class and it goes on to compute a list of the coordinates of each cell centre. 

In the case of spanwise (z-direction) averaging, a routine inside the Mesh class looks for unique pairs of streamwise and vertical coordinates (x and y directions respectively), and creates a dictionary of the index of each cell it finds. Thus for each unique value of (x, y) its stores the indexes of cells of varying z coordinates, namely `spanDict`. This yields enough information to build a 2D dimensional grid of (x,y) values 

The Mesh class also allows for a xRegion argument,`xRegion =[xMin, xMax], along a homogeneous geometry in the streamwise direction. This argument selectes a region of the 2D mesh  based on the xMin and xMax limits. 

The Field class is the used to a field file present at a specific time  folder, each line entry of the field file corresponds to each cell centre computed in the Mesh class. Once the field is loaded into an instance of the Field class, the `spanDict` dictionary can be used to average the field in the spanwise direction yielding a 2D field that is stored in the instance as an attribute.

With the 2D mesh, the user can then choose a streamwise values, x, to (linearly) interpolate the field for that specific location and  output the field at that location as function of the vertical coordinate y. The advantage of the tool is that the y-values remain consistent with the y-coordinates of the cells and only limits itself to interpolating with respect to the streamwise coordinate.

The Field class goes even further and allows the averaging in another homogenous direction, that would be the streamwise direction in the case of a channel. Yielding a one dimensional field as a function of the vertical coordinate. 


**For usage examples, please, refer to the jupyter notebook `postExample.ipynb` and `postBFS.ipynb/py/html` at the examples folder**









